// importing express module

const express = require("express");
// Creating an instance of the express
const app = express();

//importing models

const { INSTAGRAM_LOGIN , INSTAGRAM_SIGNUP } = require('./model') 

// importing body-parser module
// Returns middleware that only parses json and 
// only looks at requests where the Content-Type header matches the type option.

const bodyParser = require("body-parser")
app.use(bodyParser.json());

// import the mongoose
const mongoose = require('mongoose');
const jwt = require("jsonwebtoken")

// Returns middleware that only parses urlencoded bodies and
// only looks at requests where the Content-Type header matches the type option.

app.use(bodyParser.urlencoded({ extended: true }));

// importing validation functions from validation.js file

const {validate_register_data,_validate_password,_validate_name,validate_email} = require("./validation.js");
const { request } = require("express");


var loginUsers = []


// Declare an  array with empty
 var list_of_names = [
    
]


//.....................INSTA_ SIGN_UP....(path: "/signup")........................................



/**
 * @api {POST}  "/signup"
 * @apiName userSignup
 * @apiGroup user
 * @apiParam body-params: {String} mail -user's unique handle
 *                        {String} password -user's unique password
 *                        {String} fullname -user's full name
 *                        {String} username - username
 *                        {String} secret- user secret
 * @apiDescription user can signup by putting in above params in body 
 * of request 
 * @apiSuccess 201(created)-if account for user is created successfully  
 * @apiError 
 *            handle:
                    409(conflict)-when handle is not unique
                    406(not acceptable)-handle has more than one consecutive spaces in between  
                    406(not acceptable)-handle is less than 5 or more than 20 characters        
                    400(bad request)- when type of handle doesnt match with 
                    type in schema

             password:
                     406(not acceptable)-if password doesnt have atleast 1
                     special character or 1 number 
                     406(not acceptable)-if password is less than 8 characters and greater than 20 characters

            fullname:
                     406(not acceptable)-handle has more than one consecutive spaces in between 
                     406(not acceptable)-handle is less than 3 or more than 20 characters
                     400(bad request)- when type of handle doesnt match with type in schema  

            username:
                     406(not acceptable)-(it will check for special chars,alphabets)
                     406(not acceptable)-length is not strings,
                     409(conflict)-when username is not unique

              
 */
/**
 * This POST Method is used for signing up an instagram account for user.
 * @param requests Object from the body which has required data for signup purpose.
 * @param response sends response message for the particular condtion in which it fails or passes.
 */

// And it returns the new array

app.post("/signup", (request,response) => {
  // requests user mail from the body params

  const mail = request.body.mail
  const secret = request.body.secret
    // iterates over each element in list for checking if the mail exists already or not.

  for (let iteam of list_of_names){
        //if mail already exists sends response as "email already taken by another user"
    if (iteam.mail === mail){
      response.send("email already taken")
    } }
      // calling validate_email() for validating mail for specified conditions
  validate_email(mail)

  // requests fullname of the user from the body params
  const fullname = request.body.fullname
    //_validate_name() is used for validating the fullname for specified conditions
  _validate_name(fullname)
  // usernsme is a key  and it stores the username  in it
  
    // requests username of the user from the body params
  const username = request.body.username
    // iterates over each element in list for checking if the username exists already or not.
  for (let iteam of list_of_names){
        //if username already exists sends response as "username already taken"
    if (iteam.username === username){
      response.send("username already taken")
    } }
  // _validate_name() is used for validating the username for specified conditions

  _validate_name(username)
// requests password of the user from the body params
  const password = request.body.password
// _validate_password() is used for validating the password for specified condtions

  _validate_password(password)
  // resuests all the data as objecr from the body params
  let body_params = request.body
// validate the register data all params
let validation_result = validate_register_data(body_params)
// if there is any condition that fails sends the status code for that failed condition
if (validation_result.status_code !== 200) {
      let {status_code,status_message} = validation_result;
      console.log(status_message);
      response.sendStatus(status_code);
  } 
  // declaring a variable and storing the data for user resgistration

const x = {
      mail : (mail),
      fullname: (fullname),
      username: (username),
      password : (password),
      secret: secret
  }

// model the schema by using the mongoose.model and sotre it in the some variable


// pass the object to that variable by using new

  const insta_data = new INSTAGRAM_SIGNUP(x);

// than save the data in it. if data is saved. given the response to the console by using of "Then"

// the data saved in the mongoDB DataBase

insta_data.save().then(() => console.log('data saved'));

// push the data in the empty array for getting the data to see in the localhost

  list_of_names.push(insta_data)

  // if data is pushed in it. give the response  as username

  response.send(username)
})



//...........................INSTA_ users....(path: "/users/:fullname")...................................


/**
 * @api {GET}  /users/:fullname
 * @apiName getSpecificuser
 * @apiGroup users
 * @apiParam url-params: {String} fullname : fullname of the user
 * @apiDescription gets specific user
 * @apiSuccess 200(successful)-got specific user successfully  
 */
/**
 * @param response to get the response in localhost
 * @param request to request the body url parms
 */



app.get("/users/:fullname", (request,response) => {
 const {fullname} = request.params
  
  // model the schema by using the mongoose.model and sotre it in the some variable

  // pass the object to that variable by using new

    const insta_data = new INSTAGRAM_SIGNUP(list_of_names);

      // get the data from Database by using mongoose bulit in function find()

      INSTAGRAM_SIGNUP.find({fullname: `${fullname}`}, (error,data) => {
        if (error) {
          console.log(error)
        } else {
          response.send(data)
        }
      })



})


/**
 * @api {GET}  /users
 * @apiName getalluser
 * @apiGroup users
 * @apiDescription get all users
 * @apiSuccess 200(successful)-got all users successfully  
 */
/**
 * @param response to get the response in localhost
 * @param request to request the body url parms
 */

app.get("/users", (request,response) => {
  const {fullname} = request.params
 
 
     // model the schema by using the mongoose.model and sotre it in the some variable
 
 
     // pass the object to that variable by using new
 
       const insta_data = new INSTAGRAM_SIGNUP(list_of_names);
 
       // get the data from Database by using mongoose bulit in function find()
 
       INSTAGRAM_SIGNUP.find({}, (error,data) => {
         if (error) {
           console.log(error)
         } else {
           response.send(data)
         }
       })
 
 
 
 })
 
 

//....................INSTA_ users   (path: "/login")...........................................


/**
 * @api {POST}  "/login"  
 * @apiName userlogin
 * @apiGroup user
 * @apiParam body-params: {String} handle -user's unique mail
 *                        {String} password -user's unique password
 * @apiDescription user can login by putting in handle and password in body of request 
 * @apiSuccess 200(successful) - JWT token given if login is successful  
 * @apiError 401(uauthorized)-if handle exists and password doesnt match
             400(bad request)- if type of handle or password doesnt match the schema
             404(not found)- if user doesnt exist in db 

 */
/**
 * This POST Method is used for login an instagram account for user.
 * @param requests Object from the body which has required data for login purpose.
 * @param response sends response message for the particular condtion in which it fails or passes.
 */

app.post("/login",(request,response) => {
    // requests user mail from the body params
  const mail = request.body.mail
    // requests password of the user from the body params
  const password = request.body.password

  // model the schema by using the mongoose.model and sotre it in the some variable


  // pass the object to that variable by using new

    const insta_data = new INSTAGRAM_SIGNUP(list_of_names);
      // get the data from Database by using mongoose bulit in function find()

//and use the callback function send it result to the response
INSTAGRAM_SIGNUP.find({mail: `${mail}`}, (error,data) => {
  // if mail not in the database its throw the error in the console
  if (error) {
    console.log(error)
    // else its send data to the console and check the data if data is not their throws the error
    
  } else {
    console.log(data)
     if (data.length !== 0){
       // ittrate each one and match the password to that object.password
       for (let iteam of data){
          if (iteam.password=== password){
            var payload = {
              mail:request.body.mail
          }
          // it returns the jwt token of each user and push that token to object
          // send login is done  to the console for conformation
          const jwtToken = jwt.sign(payload,"my_insta") // TODO: upgrade payload to USER ID
            console.log("login done")
            //  declaring the object to a in a variable

            const cp  = {
              mail: mail,
              password: password,
              status_code:200,
              jwtToken: jwtToken,
              data: Date()
            }
            // pass the object to that variable by using new
              const inst = new INSTAGRAM_LOGIN(cp);
              // the data saved in the mongoDB DataBase
              inst.save().then(() => console.log('data saved'));
              // push the data to the loginUsers
              loginUsers.push(inst)
              response.send(cp)
          }else{
            response.send("plase enter the valid password")
          }
        }
        
        
     }else{
       console.log("login fail")
       response.send("email not found")
     }
  }

  
})

})



//.....................LOGIN_USERS path: "/login_users/:mail"..........................................


/**
 * @api {GET}  /users/:mail
 * @apiName getSpecificuser
 * @apiGroup users
 * @apiParam url-params: {String} mail : mail of the user
 * @apiDescription gets specific user
 * @apiSuccess 200(successful)-got specific user successfully  
 */
/**
// get an object to the array by using the  GET method
/**
 * @param response to get the response in localhost
 * @param request to get the params
 */
// And it returns the  array



app.get("/login_users/:mail", (request,response) => {
 const {mail} = request.params
  
  // pass the object to that variable by using new
   const inst = new INSTAGRAM_LOGIN(loginUsers);

      // get the data from Database by using mongoose bulit in function command find()

      INSTAGRAM_LOGIN.find({mail: `${mail}`}, (error,data) => {
        if (error) {
          console.log(error)
        } else {
          // send the data to the response
          response.send(data)
        }
      })
      

})


/**
 * @api {GET}  /login_users
 * @apiName getAllUsers
 * @apiGroup users
 * @apiDescription gets specific user
 * @apiSuccess 200(successful)-got all users successfully  
 */
/**
// get an object to the array by using the  GET method
/**
 * @param response to get the response in localhost
 * @param request to get the params
 */
// And it returns the  array



app.get("/login_users", (request,response) => {
 
     // pass the object to that variable by using new
 
       const insta_data = new INSTAGRAM_LOGIN(loginUsers);
 
       // get the data from Database by using mongoose bulit in function command find()
 
       INSTAGRAM_LOGIN.find({}, (error,data) => {
         if (error) {
           console.log(error)
         } else {
           // send the data to the response
           response.send(data)
         }
       })
       
 
 })
 
 
 
 

//...........................INSTA_DELETING_ACCOUNT....(path: "/delete_account").......................................




/**
 * @api {DELETE}  /user/delete_account
 * @apiName userDelete
 * @apiGroup user
 * @apiDescription user can delete account by putting  jwt token in header
 * @apiSuccess 200(successful)-account deleted successfully  
 * @apiError 403(forbidden)- jwt token doesnt match
 */


app.delete("/user/delete_account", (request,response) => {

  // client_key is the jwt token only logedin user are access the restaurent data

  const {client_key} = request.headers
  // pass the object to that variable by using new

    const insta_data = new INSTAGRAM_LOGIN(loginUsers);
  // fetching the user login data for validating the client_key

  INSTAGRAM_LOGIN.findOneAndRemove({jwtToken:`${client_key}`},(err,data)=>{

    // if any error in the fetching restaurant data it prints the error

    if(err){

      console.log(err)

    // else it goes through the below block

    }else{

      // if data count is 0 it means there is no loged in user

      if(data.length === 0){

        response.send("your not a login user")

      }

      // else calling the GetRestaurant function inside it the code fetches the data

      else{

        //response.send(data);

        const insta_data = new INSTAGRAM_SIGNUP(list_of_names);
 
        // get the data from Database by using mongoose bulit in function find()
  
        INSTAGRAM_SIGNUP.findOneAndRemove({mail: `${data.mail}`}, (error,data) => {
          if (error) {
            console.log(error)
          } else {
            response.send(data)
          }
        })


    
        

      }

    }

  })
})


/**
 * @api {PUT}  /user/forgot
 * @apiName userforgot
 * @apiGroup user
 * @apiParam body-params: {String} mail -user's unique mail
 *                        {String} secretAns -user's answer to secret question
 *                        {String} new_password- new password to be 
 *                         updated in db       
 * @apiDescription user can update their password by putting in handle,    secretAns and new_password in body of request
 * @apiSuccess 200(successful)-password updated successfully  
 * @apiError 
 *               mail:
                       409(conflict)-when mail doesnt exist in db
                       
               password:
                      409(conflict)-when handle and password matches with password in db,
                      406(not acceptable)-if password doesnt have atleast 1 special char,1 number 
                      406(not acceptable) -if password is less than 8 characters and greater than 20 characters    
                secret:
                      401(unauthorized)-when your secret answer doesnt match the answer in db
 */

 app.put("/user/forgot/:mail/:secret/:newpassword",(request,response)=>{
 const {mail} = request.params
 const {secret} = request.params  
 const{newpassword} = request.params
 
 const insta_data = new INSTAGRAM_SIGNUP(list_of_names);
 const x = async(mail)=>{
  try{
    const result = await INSTAGRAM_SIGNUP.updateOne({mail:`${mail}`},{

      $set:{
  
        password:`${newpassword}`
  
      }
    })
    console.log(result)
  }catch(err){
    console.log(err)
  }

}

 // get the data from Database by using mongoose bulit in function find()

 INSTAGRAM_SIGNUP.find({mail: `${mail}`},(error,data) => {
   if (error) {
     console.log(error)
   } else {
     if(data.length !== 0){
       x(mail)
     }
   }
})
 })


/**
 * @api { GET}  /posts
 * @apiName getPosts
 * @apiGroup posts     
 * @apiDescription gets 20  posts in reverse chronological order (of all people user follows)
 * @apiSuccess 200(successful)-got all posts successfully  
 * @apiError 
 *           403(forbidden)- jwt token doesnt match
 */
 app.get("/posts",(req,res)=>{
    res.send("got all posts successfully");
 })

/**
 * @api {GET}  /posts/:post_id
 * @apiName getSpecificPost
 * @apiGroup posts 
 * @apiParam url-params: {String} post_id : post_id of the post  
 * @apiDescription gets specific post
 * @apiSuccess 200(successful)-got specific post successfully  
 * @apiError 
 *           403(forbidden)- jwt token doesnt match
 *           post_id:
             406(not acceptable)-if post_id doesnt exist in db
             401(unauthorized)-when post is not public and you dont have access
      
 */
 app.get("/posts/:post_id",(req,res)=>{
  res.send("got specific post successfully");
})



/**
 * @api {POST}  /posts
 * @apiName uploadPost
 * @apiGroup posts 
 * @apiParam 
 *           header-param:{String} authorization: token - generated token 
 *                         during login 
 *           body-params: {String} handle-user's unique handle
 *                        {Boolean}accessibility-tells the public or private 
 *                         status of post
 *                        {String}caption-text content of post
 *                        
 * @apiDescription user can upload their post
 * @apiSuccess 200(successful)-post uploaded successfully   
 * @apiError 
 *            handle:
                    409(conflict)-when handle doesnt exist in db
              
       accessibility:
                    406(not acceptable)-when the type is not same as in schema

             caption:
                    406(not acceptable)-when the type is not same as in 
                    schema
                    406(not acceptable)-when capiton is greater than 100 characters      

      
 */
  app.post("/posts",(req,res)=>{


              res.send(" post uploaded successfully");
  })


  app.listen(3000)
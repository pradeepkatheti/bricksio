
const { response, request } = require("express");
const express = require("express");
const app = express();
//const fetch = require("node-fetch")
const bodyParser = require("body-parser")
app.use(bodyParser.json());
const {v4: uuidv4} = require("uuid")

app.use(bodyParser.urlencoded({ extended: true }));



// Declare an  array with an Object
 var list_of_names = [
    
]

// post an object to array by using the  POST method

// And it returns the new array

app.post("/post", (request,response) => {
  const name = request.body.name
  const id = uuidv4()
  
  const x = {
    name: name,
    id : id 
    
  }
  list_of_names.push(x)
  response.send(name)
})

// getting data from an array By using GET method

// runing a code in localhost wirh port number

// by using get method


app.get("/", async (request, response) => {
  
  response.send(list_of_names)

  });
// delete an object in array by using the  DELETE method

// by the using id iterate each one

// finding an index and delete that item by using splice method 

// It returns the new array

app.delete("/name/:id", (request,response) => {
  const {id} = request.params
  for (let name1 of list_of_names){
    if (name1.id === parseInt(id)) {
      let index_o = list_of_names.indexOf(name1)
      list_of_names.splice(index_o,1)
    }
    
  }
  // sending the response
  response.send(list_of_names)
  
  
})

// update an name in an object by using the put method

// And getting the name and id in body

// and the update the values

// It returns the new array



app.put('/name/:id/:neww',(req,res)=>{ 
       
  const name = request.body.name
  const {id} = request.params
  const {neww} =request.params
  
      const obj = list_of_names.find((obj)=>obj.id === parseInt(id));
   if(name){      
       obj.name = neww 
     } 
        
         response.send(`user with id ${id} has been updated sucessfully`)});


// the port number is "3030"
app.listen(3030)

